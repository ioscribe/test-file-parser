defmodule Razoyo do
  def main(input) do
    list =
      load(input)
      |> clean

    create_output_directory()
    generate_products_csv(list)
    generate_customers_xml(list)
    generate_orders_json(list)
  end

  def load(file_name) do
    if File.read(file_name) == {:error, :enoent} do
      exit("The requested file does not exist.")
    else
      {:ok, data} = File.read(file_name)
      data
    end
  end

  def clean(data) do
    String.replace(data, "\"", "")
    |> String.split("\n", trim: true)
    |> Enum.map(fn x -> String.split(x, ",") end)
  end

  # PARSE LIST BY TYPE ////////////////////////////////////////////////////////////////
  def parse(["product", sku, name, brand, price, currency]) do
    %{sku: sku, name: name, brand: brand, price: price, currency: currency}
  end

  def parse(["customer", id, name, email, age, gender]) do
    %{id: id, name: name, email: email, age: age, gender: gender}
  end

  def parse(["order", id, line_numbers, subtotal, tax, total, customer_id]) do
    %{
      id: id,
      head: %{
        line_numbers: line_numbers,
        subtotal: subtotal,
        tax: tax,
        total: total,
        customer: customer_id
      }
    }
  end

  def parse(["order-line", line_number, product_name, price, quantity]) do
    line_number = String.to_integer(line_number)
    quantity = String.to_integer(quantity)

    %{
      position: line_number,
      product_name: product_name,
      price: price,
      quantity: quantity,
      row_total: :erlang.float_to_binary(String.to_float(price) * quantity, [{:decimals, 2}])
    }
  end

  # ORDERS //////////////////////////////////////////////////////////////////////////////////
  def generate_orders_json(list) do
    output =
      get_orders(list)
      |> Enum.map(&create_order_map(&1, list))

    json =
      %{orders: output}
      |> Poison.encode!()

    File.write!("output_files/orders.json", json)
    IO.puts("Successfully generated order list")
  end

  def get_orders(list) do
    Enum.filter(list, fn x -> List.first(x) == "order" end)
  end

  def create_order_map(order, list) do
    order_map = parse(order)

    lines =
      fetch_order_lines(order, list)
      |> Enum.map(&parse(&1))

    Map.put(order_map, :lines, lines)
  end

  def fetch_order_lines(order, list) do
    order_index = Enum.find_index(list, &(&1 == order))
    {:ok, line_count} = Enum.fetch(order, 2)

    line_count = String.to_integer(line_count)

    fetch_order_lines(list, order_index, line_count)
  end

  def fetch_order_lines(_, _, 0, lines) do
    lines
  end

  def fetch_order_lines(list, order_index, lines_remaining, lines \\ []) do
    {:ok, line} = Enum.fetch(list, order_index + lines_remaining)
    lines = lines ++ [line]

    fetch_order_lines(list, order_index, lines_remaining - 1, lines)
  end

  # CUSTOMERS //////////////////////////////////////////////////////////////////////////
  def get_customers(list) do
    Enum.filter(list, fn x -> List.first(x) == "customer" end)
    |> Enum.map(&parse(&1))
  end

  def generate_customers_xml(list) do
    customers = get_customers(list)

    customer_xml_objects =
      Enum.map(customers, fn x ->
        "<customer>" <>
          "<id>#{x.id}</id>" <>
          "<name>#{x.name}</name>" <>
          "<email>#{x.email}</email>" <>
          if x.age === "" do
            ""
          else
            "<age>#{x.age}</age>"
          end <>
          "<gender>#{x.gender}</gender>" <>
          "</customer>"
      end)

    output =
      "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" <>
        "<customers>" <>
        "#{customer_xml_objects}" <>
        "</customers>"

    File.write!("output_files/customers.xml", output)
    IO.puts("Successfully generated customer list")
  end

  # PRODUCTS //////////////////////////////////////////////////////////////////////////////////////////////////////

  def generate_products_csv(list) do
    csv_header = ~s("Sku","Name","Brand","Price","Currency"\r\n)

    csv_data =
      Enum.filter(list, fn x -> List.first(x) == "product" end)
      |> Enum.map(&join_products(&1))

    output = csv_header <> Enum.join(csv_data, "\r\n")
    File.write!("output_files/products.csv", output)
    IO.puts("Successfully generated product list")
  end

  def join_products([_product, sku, name, brand, price, currency]) do
    default_currency =
      if currency == "" do
        "USD"
      else
        currency
      end

    csv_list =
      [~s("#{sku}"), ~s("#{name}"), ~s("#{brand}"), "#{price}", default_currency]
      |> Enum.join(",")

    csv_list
  end

  def create_output_directory do
    {status, _} = File.stat("output_files")

    case status do
      :ok ->
        nil

      :error ->
        File.mkdir!("output_files")
        IO.puts("Created output_files directory")
    end
  end
end
