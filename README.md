## Installation

To run the program, you must first install the Elixir language by following the instructions on [this page](https://elixir-lang.org/install.html)

Once you have installed Elixir, you can simply clone the git repository and
make the `razoyo` binary executable.
```
git clone https://gitlab.com/ioscribe/test-file-parser.git
cd test-file-parser
chmod +x bin/razoyo
```

# Usage

To parse data, simply reference the program and the text file you wish to parse.

```
./bin/razoyo test-file.txt
```

The generated files will be placed in an `output_files` directory inside your
current working directory.

For example, if you run the program from your home directory:

```
cd
./Downloads/test-file-parser/bin/razoyo input_file.txt
```

Your output files would be located in:

```
~/output_files
```